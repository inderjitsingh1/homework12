import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;

import com.google.gson.*;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class WxStation extends Application
{
	
      TextField ZipCode;
      Button CheckWeather;
      Label Weather;
   
      @Override
      public void start(Stage primaryStage) 
      {
      
      ZipCode= new TextField("Enter the Zip Code");
      CheckWeather= new Button("Check weather");
      Weather= new Label("Weather");
      
      ZipCode.setAlignment(Pos.CENTER);
      Weather.setAlignment(Pos.CENTER);
      Weather.setStyle("-fx-border-color: #000; -fx-padding: 5px;");
            
      VBox root = new VBox();
      
      root.setAlignment(Pos.CENTER);
      
      root.setSpacing(15);
      
      root.getChildren().add(ZipCode);
      root.getChildren().add(CheckWeather);
      root.getChildren().add(Weather);
      
      
      setWidths();
      attachCode();
   
      
      Scene scene = new Scene(root, 400, 350);
      primaryStage.setTitle("Inderjit Singh");
      primaryStage.setScene(scene);
      primaryStage.show();
      
      }
      
      public void setWidths()
      {
       // Set widths of all controls
       ZipCode.setMaxWidth(220);
       CheckWeather.setPrefWidth(150);
   
       Weather.setMaxWidth(320);
       Weather.setMinHeight(210);
      }
      
     public void attachCode()
     {
       // Attach actions to buttons
       CheckWeather.setOnAction(e -> GetWeather(e));
     }
     
     public void GetWeather(ActionEvent e)
     {
        WxStation wx= new WxStation();
        String w=wx.getWx(ZipCode.getText());
        Weather.setText(w);
         
     }
   
   
   
   
   
   public String getWx(String ZipCode)
	{
		JsonElement jse = null;
      String wxReport = null;

		try
		{
			// Construct WxStation API URL
			URL wxURL = new URL("http://api.openweathermap.org/data/2.5/weather?zip="
					+ ZipCode
					+ "&units=imperial&appid=3bd82fafb6b5fd747604ed7c6991e4c4");

			// Open the URL
			InputStream is = wxURL.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jse = new JsonParser().parse(br);
      
			// Close the connection
			is.close();
			br.close();
		}
		catch (java.io.UnsupportedEncodingException uee)
		{
			uee.printStackTrace();
		}
		catch (java.net.MalformedURLException mue)
		{
			mue.printStackTrace();
		}
		catch (java.io.IOException ioe)
		{
			ioe.printStackTrace();
		}

		if (jse != null)
		{
      
           if(jse.getAsJsonObject().get("cod").getAsInt()==404)
             {
               wxReport= "ERROR: No cities match your search query";
             }
           else
             {
               
               String Location = jse.getAsJsonObject().get("name").getAsString();
               wxReport =  "Location:           " + Location ;
              
               String Country= jse.getAsJsonObject().get("sys").getAsJsonObject().get("country").getAsString();
               wxReport = wxReport+ ", "+ Country+ "\n" ;
         
               JsonArray obs = jse.getAsJsonObject().get("weather").getAsJsonArray();
               String Weather = obs.get(0)
                                .getAsJsonObject().get("description").getAsString();
               wxReport = wxReport + "Weather:           " + Weather + "\n";
               
               String Temperature = jse.getAsJsonObject().get("main").getAsJsonObject().get("temp").getAsString();
               wxReport =wxReport + "Temperature F: " + Temperature + "\n";
         
               double WindSpeedInMPH=  jse.getAsJsonObject().get("wind").getAsJsonObject().get("speed").getAsInt() *2.23694;
               WindSpeedInMPH= Math.round(WindSpeedInMPH * 100.0) / 100.0;
               String Wind= "From " +jse.getAsJsonObject().get("wind").getAsJsonObject().get("deg").getAsString() + " degrees at " +  WindSpeedInMPH + " MPH";
               wxReport =wxReport+ "Wind:                 "+ Wind + "\n";
               
               double PressureinHG= jse.getAsJsonObject().get("main").getAsJsonObject().get("pressure").getAsInt()* 0.029529983071445;
               PressureinHG= Math.round(PressureinHG * 100.0) / 100.0;
               wxReport =wxReport+ "Pressure inHG: " + PressureinHG+ "\n";
		      }
      }
    return wxReport;
	}

	public static void main(String[] args)
	{
		/*WxStation b = new WxStation();
		String wx = b.getWx("95991");
    if ( wx != null )
		  System.out.println(wx);*/
        
        launch(args);
	}
}
