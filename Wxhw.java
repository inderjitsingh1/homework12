import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;

import com.google.gson.*;

import java.sql.Timestamp;    
import java.util.Date;    
import java.text.SimpleDateFormat;

public class Wxhw 
{
	
        
   public String getWx(String ZipCode)
	{
		JsonElement jse = null;
      String wxReport = null;

		try
		{
			// Construct WxStation API URL
			URL wxURL = new URL("http://api.openweathermap.org/data/2.5/weather?zip="
					+ ZipCode
					+ "&units=imperial&appid=3bd82fafb6b5fd747604ed7c6991e4c4");

			// Open the URL
			InputStream is = wxURL.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jse = new JsonParser().parse(br);
      
			// Close the connection
			is.close();
			br.close();
		}
		catch (java.io.FileNotFoundException erro)
      {
            System.out.println("ERROR: No cities match your search query\n");
      }
      catch (java.io.UnsupportedEncodingException uee)
		{
			uee.printStackTrace();
		}
		catch (java.net.MalformedURLException mue)
		{
			mue.printStackTrace();
		}
		catch (java.io.IOException ioe)
		{
			ioe.printStackTrace();
		}

		if (jse != null)
		{
      
           if(jse.getAsJsonObject().get("cod").getAsInt()==404)
             {
               wxReport= "ERROR: No cities match your search query";
             }
           else
             {
               
                String Location = jse.getAsJsonObject().get("name").getAsString();
               wxReport =  "Location:      " + Location ;
              
               String Country= jse.getAsJsonObject().get("sys").getAsJsonObject().get("country").getAsString();
               wxReport = wxReport+ ", "+ Country+ "\n" ;
         
               long Unixdate=jse.getAsJsonObject().get("dt").getAsLong();
               Date date = new java.util.Date(Unixdate*1000L); 
               SimpleDateFormat sdf = new java.text.SimpleDateFormat("MMMMM d, YYYY H:m z "); 
               String formattedDate = sdf.format(date);
               wxReport = wxReport+ "Time:          Last update on "+  formattedDate+ "\n";
               
               JsonArray obs = jse.getAsJsonObject().get("weather").getAsJsonArray();
               String Weather = obs.get(0)
                                .getAsJsonObject().get("description").getAsString();
               wxReport = wxReport + "Weather:       " + Weather + "\n";
               
               String Temperature = jse.getAsJsonObject().get("main").getAsJsonObject().get("temp").getAsString();
               wxReport =wxReport + "Temperature F: " + Temperature + "\n";
         
               double WindSpeedInMPH=  jse.getAsJsonObject().get("wind").getAsJsonObject().get("speed").getAsInt() *2.23694;
               WindSpeedInMPH= Math.round(WindSpeedInMPH * 100.0) / 100.0;
               String Wind= "From " +jse.getAsJsonObject().get("wind").getAsJsonObject().get("deg").getAsString() + " degrees at " +  WindSpeedInMPH + " MPH";
               wxReport =wxReport+ "Wind:          "+ Wind + "\n";
               
               double PressureinHG= jse.getAsJsonObject().get("main").getAsJsonObject().get("pressure").getAsInt()* 0.029529983071445;
               PressureinHG= Math.round(PressureinHG * 100.0) / 100.0;
               wxReport =wxReport+ "Pressure inHG: " + PressureinHG+ "\n";		      }
      }
    return wxReport;
	}

	public static void main(String args[])
	{
		if(args.length==0)
      {
         System.out.println("Provide a zip code\n");
      }
      
      else
      {
         Wxhw b = new Wxhw();
   		String wx = b.getWx(args[0]);
   		if (wx != null)
         {
            System.out.println(wx);
         }
      }
        
	}
}
